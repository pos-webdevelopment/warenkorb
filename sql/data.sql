drop  database if exists php23;
create database php23;
use php23;
CREATE TABLE books(
   bId INTEGER  NOT NULL auto_increment,
   bTitle VARCHAR(255) NOT NULL,
   bPrice NUMERIC(5,2) NOT NULL,
   bStock INTEGER  NOT NULL,
   primary key(bId),
   unique(bId, bTitle)
);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Y-Solowarm',6.25,8);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Gembucket',7.80,3);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Coding für Dummies',15.00,9);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Bamity',27.15,4);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Bytecard',11.81,4);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Zoolab',5.83,7);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Keylex',30.35,7);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Mat Lam Tam',24.44,1);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Holdlamis',10.31,0);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Viva',15.78,7);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Lotstring',14.66,2);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Holdlamis',16.79,6);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Mat Lam Tam',38.28,4);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Sonair',9.48,10);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Duobam',29.27,7);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Overhold',24.83,6);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Hatity',5.41,1);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Latlux',37.97,8);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('Cardguard',26.13,6);
INSERT INTO books(bTitle,bPrice,bStock) VALUES ('FlexbIdy',15.27,1);

select * from books;

CREATE TABLE cart(
   cId INTEGER  NOT NULL auto_increment,
   cUserId INTEGER NULL,
   bId INTEGER NOT NULL,
   cAmount INTEGER  NOT NULL, 
   primary key(cId), 
   unique(bId, cAmount),
   foreign key(bId) references books(bId)
);

ALTER TABLE cart ADD constraint id unique(bId);

-- INSERT INTO cart(bId, cAmount) VALUES(2, 6);
-- INSERT INTO cart(bId, cAmount) VALUES(3, 1);
-- INSERT INTO cart(bId, cAmount) VALUES(4, 10);

-- SELECT c.cId, b.bTitle, c.cAmount, b.bPrice FROM cart c INNER JOIN books b ON c.bId = b.bId WHERE c.cId = 1;
-- SELECT count(*) FROM cart ;
-- select * from cart;
-- SELECT c.bId, bTitle, c.cAmount FROM cart c INNER JOIN books b ON c.bId = b.bId;

-- UPDATE cart SET cAmount = 2 WHERE bId = 1;

-- DELETE FROM cart WHERE bId = 2;
-- SET SQL_SAFE_UPDATES = 0;
-- UPDATE books b, cart c INNER JOIN books SET b.bStock = (b.bStock - c.cAmount) WHERE b.bId = c.bId;
-- DELETE FROM cart;
-- ALTER TABLE cart AUTO_INCREMENT=1;
-- SET SQL_SAFE_UPDATES = 1;

