<?php
require_once "Book.php";
$host = 'localhost';
$username = 'root';
$passwd = '';
$dbname = 'php23';
$connect = new mysqli($host, $username, $passwd, $dbname);
class Cart
{
    public static function getNumberOfItems() // $userId
    {
        global $connect;
        $query = "SELECT * FROM cart;"; //WHERE cUserId=$userId
        $result = $connect->query($query);
        return mysqli_num_rows($result);
    }

    public static function addItem($id, $count)
    {
        global $connect;
        $query = "INSERT INTO cart(bId, cAmount) VALUES($id, $count);";
        $success = $connect->query($query);
        if ($success) {
            return true;
        } else { // Item already saved in cart
            return false;
        }
    }

    public static function updateItem($id, $count)
    {
        if (self::getById($id)['bTitle'] != null) {
            global $connect;
            $query = "UPDATE cart SET cAmount = $count WHERE cId = $id;";
            $success = $connect->query($query);
            if ($success) {
                return true;
            }
        } else {
            return false;
        }
    }

    public static function getById($id)
    {
        global $connect;
        $dataById = array();
        $query = "SELECT c.cId, b.bTitle, c.cAmount, b.bPrice, b.bStock FROM cart c
                    INNER JOIN books b ON c.bId = b.bId WHERE c.cId = $id;";
        $result = $connect->query($query);
        if ($result == false) {
            echo $connect->error;
        } else {
            $row = $result->fetch_object();
            $dataById['cId'] = $row->cId;
            $dataById['bTitle'] = $row->bTitle;
            $dataById['cAmount'] = $row->cAmount;
            $dataById['bPrice'] = $row->bPrice;
            $dataById['bStock'] = $row->bStock;
        }
        return $dataById;
    }

    public static function isInCart($id) {
        global $connect;
        $dataById = array();
        $query = "SELECT cAmount FROM cart  WHERE bId = $id;";
        $result = $connect->query($query);
        if ($result == false) {
            echo $connect->error;
        } else {
            $row = $result->fetch_object();
            $dataById['cAmount'] = $row->cAmount;
        }
        if($dataById['cAmount']>0) {
            return $dataById['cAmount'];
        } else {
            return false;
        }
    }

    public static function deleteItem($id)
    {
        global $connect;
        $query = "DELETE FROM cart WHERE cId = $id;";
        $success = $connect->query($query);
        if ($success) {
            return true;
        } else {
            return false;
        }
    }

    public static function orderAndCleanCart() {
        global $connect;
        $query = "SET SQL_SAFE_UPDATES = 0;";
        $connect->query($query);
        $query = "UPDATE books b, cart c INNER JOIN books SET b.bStock = (b.bStock - c.cAmount) WHERE b.bId = c.bId;";
        $connect->query($query);
        $query = "DELETE FROM cart;";
        $result = $connect->query($query);
        $query = "ALTER TABLE cart AUTO_INCREMENT=0;";
        $connect->query($query);
        $query = "SET SQL_SAFE_UPDATES = 1;";
        $connect->query($query);
        if($result) {
            return true;
        } else {
            return false;
        }
    }
}