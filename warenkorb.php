<?php
session_start();
error_reporting(0);
//$_SESSION['userId'] = 1;
require_once "Cart.php";
if (isset($_POST['delete'])) {
    if (Cart::deleteItem($_POST['id'])) {
        ?>
        <div class="alert alert-info text-center">
            <p>Artikel wurde erfolgreich gelöscht.</p>
        </div>
        <?php
    } else {
        ?>
        <div class="alert alert-danger text-center">
            <p>Artikel konnte nicht gelöscht werden.</p>
        </div>
        <?php
    }
}
if (isset($_POST['add'])) {
    $items = Cart::getById($_POST['id'])['cAmount'];
    $maxItems = Cart::getById($_POST['id'])['bStock'];
    $items++;
    if ($items <= $maxItems && Cart::updateItem($_POST['id'], $items)) {
        ?>
        <div class="alert alert-info text-center">
            <p>Anzahl des Artikels wurde erfolgreich erhöht.</p>
        </div>
        <?php
    } else {
        ?>
        <div class="alert alert-danger text-center">
            <p>Anzahl konnte nicht erhöht werden (max. Lagerbestand erreicht). </p>
        </div>
        <?php
    }
}
if (isset($_POST['reduce'])) {
    $items = Cart::getById($_POST['id'])['cAmount'];
    $items--;
    if ($items >= 1 && Cart::updateItem($_POST['id'], $items)) {
        ?>
        <div class="alert alert-info text-center">
            <p>Anzahl des Artikels wurde erfolgreich reduziert.</p>
        </div>
        <?php
    } else {
        ?>
        <div class="alert alert-danger text-center">
            <p>Es kann nicht weniger als 1 Artikel bestellt werden. Bitte benutzen Sie den Löschen-Button, falls Sie den Artikel entfernen möchten!</p>
        </div>
        <?php
    }
}
if (isset($_POST['ordered'])) {

    if (Cart::orderAndCleanCart()) {
        ?>
        <div class="alert alert-success text-center">
            <p>Bestellung erfolgreich abgeschickt.</p>
        </div>
        <?php
    } else {
        ?>
        <div class="alert alert-danger text-center">
            <p>Bestellung konnte nicht übernommen werden.</p>
        </div>
        <?php
    }
}
?>
<html lang="de">
<head>
    <title>Warenkorb</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        div#item:hover {
            background-color: #fff3cd;
            transition: background-color 0.4s ease-in-out;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row sticky-top rounded" style="background-color: rgba(255,255,255, 0.75)">
        <div class="col-sm-6">
            <h1 class="mb-5 mt-3">Warenkorb</h1>
        </div>
        <div class="col-sm-6">
            <a href="index.php" class="btn btn-outline-warning mb-5 mt-3 float-right" id="back">Zurück zum Shop</a>
        </div>
    </div>
    <div class="row alert alert-dark">
        <div class="col-sm-3">
            <strong>Buchtitel</strong>
        </div>
        <div class="col-sm-2 text-center">
            <strong>Anzahl</strong>
        </div>
        <div class="col-sm-2 text-center">
            <strong>Einzelpreis</strong>
        </div>
        <div class="col-sm-3 text-center">
            <strong>Gesamtpreis</strong>
        </div>
        <div class="col-sm-2 text-center">
            <strong>Edit</strong>
        </div>
    </div>
    <?php
    if (isset($_POST['ordered'])) {
        ?>
        <div class="alert alert-success text-center">
            <strong class="text-success">Einkauf erfolgreich abgeschlossen!</strong>
            <p>Vielen Dank & auf Wiedersehen</p>
            <a href="index.php">Einkauf fortsetzen</a>
        </div>
        <?php
    } else {
        $totalPrice = 0;
        for ($i = 1; $i <= 20; $i++) {
            if (Cart::getById($i)['cId'] != null) {
                ?>
                <div class="row alert alert-light" id="item">
                    <div class="col-sm-3">
                        <p><?= Cart::getById($i)['bTitle'] ?></p>
                    </div>
                    <div class="col-sm-2 text-center">
                        <p><?= Cart::getById($i)['cAmount'] ?></p>
                    </div>
                    <div class="col-sm-2 text-center">
                        <p>€ <?= Cart::getById($i)['bPrice'] ?></p>
                    </div>
                    <div class="col-sm-3 text-center">
                        <p>€ <?php
                            $price = Cart::getById($i)['bPrice'];
                            $amount = Cart::getById($i)['cAmount'];
                            $total = $price * $amount;
                            $totalPrice += $total;
                            echo $total;
                            ?></p>
                    </div>
                    <div class="col-sm-2 text-center">
                        <form action="warenkorb.php" method="post">
                            <input type="hidden" name="id" value="<?= Cart::getById($i)['cId'] ?>">
                            <button class="btn btn-success  mb-1" name="add" type="submit" title="Anzahl um 1 erhöhen">
                                <img src="./open-iconic/svg/caret-top.svg" alt="+">
                            </button>
                            <button class="btn btn-warning mb-1" name="reduce" type="submit"
                                    title="Anzahl um 1 verringern">
                                <img src="./open-iconic/svg/caret-bottom.svg" alt="-">
                            </button>
                            <button class="btn btn-danger  mb-1" name="delete" type="submit" title="Artikel entfernen">
                                <img src="./open-iconic/svg/trash.svg" alt="delete">
                            </button>


                        </form>

                    </div>
                </div>
                <?php
            }
        }
        if (Cart::getNumberOfItems() > 0) {
            ?>
            <div class="row alert alert-dark">
                <div class="col-sm-6">
                    <strong>Gesamtpreis:</strong>
                </div>
                <div class="col-sm-4 text-center">
                    <strong>€ <?= $totalPrice ?></strong>
                </div>
                <div class="col-sm-2 align-content-center">
                    <form method="post" action="warenkorb.php">
                        <button type="submit" name="ordered" id="ordered" class="float-right btn btn-success">Bestellen
                        </button>
                    </form>
                </div>
            </div>

            <?php
        } else {
            ?>
            <div class="alert alert-warning text-center">
                <p>Keine Elemente im Warenkorb vorhanden!</p>
                <a href="index.php">Einkauf fortsetzen</a>
            </div>
            <?php
        }
    }
    ?>
</div>

</body>
</html>