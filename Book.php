<?php


class Book
{
    private $bId = 0;
    private $bTitle = "";
    private $bPrice = 0.00;
    private $bStock = 0;

    /**
     * Book constructor.
     */
    public function __construct()
    {
    }


    private function connect() {
        $host = 'localhost';
        $username = 'root';
        $passwd = '';
        $dbname = 'php23';
        return new PDO("mysql:host=$host;dbname=$dbname", $username, $passwd);
    }

    public static function getAllBooks() {
        $con = self::connect();
        $sql = "SELECT *  FROM books;";
        $query = $con->prepare($sql);
        $query->execute();
        $bookList = $query->fetchAll(PDO::FETCH_CLASS, "Book");
        $books = [];
        foreach ($bookList as $b) {
            $books[] = $b;
        }
        return $books;
    }

    public static function getById($bId) {
        $con = self::connect();
        $sql = "SELECT *  FROM books WHERE bId = ?;";
        $query = $con->prepare($sql);
        $query->execute(array($bId));
        return $query->fetchObject("Book");
    }



    /**
     * @return int
     */
    public function getBId()
    {
        return $this->bId;
    }

    /**
     * @param int $bId
     */
    public function setBId($bId)
    {
        $this->bId = $bId;
    }

    /**
     * @return string
     */
    public function getBTitle()
    {
        return $this->bTitle;
    }

    /**
     * @param string $bTitle
     */
    public function setBTitle($bTitle)
    {
        $this->bTitle = $bTitle;
    }

    /**
     * @return float
     */
    public function getBPrice()
    {
        return $this->bPrice;
    }

    /**
     * @param float $bPrice
     */
    public function setBPrice($bPrice)
    {
        $this->bPrice = $bPrice;
    }

    /**
     * @return int
     */
    public function getBStock()
    {
        return $this->bStock;
    }

    /**
     * @param int $bStock
     */
    public function setBStock($bStock)
    {
        $this->bStock = $bStock;
    }


}