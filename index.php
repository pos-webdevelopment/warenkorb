<?php
session_start();
error_reporting(0);
//$_SESSION['userId'] = 1;
require_once "Book.php";
require_once "Cart.php";
if (isset($_POST['submit'])) {
    if (!(Cart::addItem($_POST['id'], $_POST['count']))) {
        ?>
        <div class="alert alert-danger text-center">
            <p>Der Artikel konnte nicht in den Warenkorb gelegt werden!</p>
        </div>
        <?php
    }
}
?>
<html>
<head>
    <title>Warenkorb</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row sticky-top rounded" style="background-color: rgba(255,255,255, 0.75)">
        <div class="col-sm-6">
            <h1 class="mb-5 mt-3">Bücher-Shop</h1>
        </div>
        <div class="col-sm-6">
            <a href="warenkorb.php" class="btn btn-outline-success mb-5 mt-3 float-right" id="back">
                Zum Warenkorb (<?= Cart::getNumberOfItems() ?> Artikel)</a>
        </div>
    </div>
    <?php
    $currentBook = Book::getById(10);
    //echo $currentBook->getBTitle();
    $allBooks = Book::getAllBooks();
    $counter = 0;
    foreach ($allBooks as $b) {
    $counter++;
    $currentBook = Book::getById($counter);
    ?>
    <div class="row alert <?= Cart::isInCart($counter) ? 'alert-success' : 'alert-primary' ?>">
        <div class="col-sm-12">
            <strong><?= $currentBook->getBTitle() ?></strong>
        </div>
        <div class="col-sm-2">
            <span>€ <?= $currentBook->getBPrice() ?></span>
        </div>
        <div class="col-sm-10">
            <?php
            if ($currentBook->getBStock() <= 0) {
                echo "<p class='alert alert-danger text-center'>Artikel derzeit leider nicht verfügbar!</p>";
            } else {
                ?>
                <form action="index.php" method="POST">
                    <input type="hidden" name="id" value="<?= $currentBook->getBId() ?>">
                    <label for="count" style="padding-right: 10px">Menge:</label>
                    <select name="count" id="<?= 'count' . $counter ?>" class="col-sm-6 custom-select"
                            onchange="enableButton(<?= $counter ?>)" <?= Cart::isInCart($counter) ? 'disabled' : '' ?>>
                        <?php
                        if (Cart::isInCart($counter)) {
                            echo '<option value=' . Cart::isInCart($counter)['cAmount'] . '>' . Cart::isInCart($counter)['cAmount'] . '</option>';
                        } else {
                            ?>
                            <option value="0">-- Bitte auswählen --</option>
                            <?php
                            for ($a = 1; $a <= $currentBook->getBStock(); $a++) {
                                echo '<option value="' . $a . '">' . $a . '</option>';
                            }
                        }
                        ?>
                    </select>
                    <button name="submit" id="<?= 'submitOrder' . $counter ?>" disabled
                            class="btn btn-info float-right">Hinzufügen
                    </button>
                    <?php
                    if (Cart::isInCart($counter)) {
                        ?>
                        <div class="col-sm-9">
                            <a href="warenkorb.php" class="text-success">Warenkorb
                                (<?= Cart::isInCart($counter)['cAmount'] ?>) bearbeiten</a>
                        </div>
                        <?php
                    }
                    ?>
                </form>
                <?php
            }
            ?>
        </div>
    </div>
    <script>
        function enableButton(val) {
            if (document.getElementById("count" + val).value !== "0") {
                document.getElementById("submitOrder" + val).disabled = false;
            } else {
                document.getElementById("submitOrder" + val).disabled = true;
            }
        }
    </script>
<?php
    }
    ?>
    <a href="warenkorb.php" type="submit" name="order" class="btn btn-success float-right">
        Zum Warenkorb (<?= Cart::getNumberOfItems() ?> Artikel)</a>
</div>
<br><br><br>
<footer class="card-footer">
    <div class="row mb-12">
        <div class="col-sm-5">
            <p>(c) 12/2020, Design by AZ</p>
        </div>
        <div class="col-sm-6">
            <img src="logo.png" alt="az" class="img-fluid text-center">
        </div>
        <div class="col-sm-1">
            <a href="sql/reset.php">Reset web store</a>
        </div>

    </div>
</footer>
</body>
</html>